import React, { Component } from 'react'
import ReactDOM from 'react-dom'


class UserRuns extends Component {

  constructor(props) {
    super(props);

    this.state = {
      runs: []
    }

    // Bind Methods
    this.fetchSuccess = this.fetchSuccess.bind(this);
    this.renderRun = this.renderRun.bind(this);
  }

  supportPath() {
    return App.supportApi() + 'v1/';
  }


  clientPath() {
    return App.clientApi() + 'v1/';
  }

  
  componentWillMount() {
    if (this.props.userId) {
      this.getRuns(this.props.userId);
    }
  }
  

  componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      this.getRuns(nextProps.userId);
    }
  }


  componentDidUpdate(prevProps, prevState) {
    
    if (this.props.userId && this.state.runs.length !== prevState.runs.length) {

      // Scroll to the table
      let runTable = ReactDOM.findDOMNode(this);
      $('html, body').clearQueue().animate({
          scrollLeft: '0px',
          scrollTop: ($(runTable).offset().top-200) + 'px'
      }, 'fast');
    }
  }


  //
  // Network fetch functions
  //
  fetch(apiPath, userId) {
    const path = this.supportPath() + apiPath + '?user_id=' + encodeURIComponent(userId);
    console.log('Fetch: ', this.supportPath() + apiPath);
    var that = this;
    $.ajax({
      url: path,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  }


  fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({runs: data.runs || []});
    }
  }


  fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Run Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Run Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Run search failed.");
      console.error(err);
    }
  }


  renderRun(run) {
    var that = this;
    return (
      <tr key={ run.run_id }>
        <td>{ this.props.runLink(run.run_id) }</td>
        <td>{ run.created_date }</td>
        <td>{ run.restaurant_name }</td>
        <td>{ run.run_state }</td>
        <td>{ run.run_status }</td>
        <td>{ run.additional_info }</td>
        <td>{ run.order_count }</td>
        <td>{ run.flags.indexOf('markedup_prices') >= 0 ? 'Marked Up' : 'No' }</td>
        <td>${ run.earnings.toFixed(2) }</td>
        <td>${ run.reimburse_amount.toFixed(2) }</td>
      </tr>
    );
  }


  render() {
    var runRows = this.state.runs.map(this.renderRun);

    if (runRows.length && this.props.userId) {
      return ( <div className="user-runs">
        <h3>{ this.props.displayName } Runs:</h3>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>Run ID</th>
              <th>Created Date</th>
              <th>Restaurant</th>
              <th>Run State</th>
              <th>Status</th>
              <th>Reimbursement Held Back</th>
              <th>Orders</th>
              <th>Markedup Prices</th>
              <th>Earnings</th>
              <th>Reimburse Value</th>
            </tr>
          </thead>
          <tbody>
            { runRows }
          </tbody>
        </table>
      </div>);
    }
    else if (this.props.userId) {
      return (<i>No Runs Found</i>);
    }
    else {
      return (<div className="user-runs"></div>);
    }
  }


  getRuns(userId) {
    if (!userId) {
      return;
    }
    var path = 'dashboard/runs';
    this.fetch(path, userId);
  }
}

module.exports = UserRuns;
