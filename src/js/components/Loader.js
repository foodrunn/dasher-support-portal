var React = require('react');

var Loader = React.createClass({

  getInitialState: function() {
    return {
      active: false
    }
  },


  componentWillMount: function() {
    var that = this;

    $(document).ajaxStart(() => {
      that.setState({ active: true });
    });

    $(document).ajaxComplete(() => {
      that.setState({ active: false });
    });

    $(document).on('loading',(evt, val) => {
      that.setState({ active: val });
    });
  },


  render: function() {
    var loaderStyle = {
      display: this.state.active ? 'block' : 'none'
    };
    return (
      <div className="sk-cube-grid" style={loaderStyle}>
        <div className="sk-cube sk-cube1"></div>
        <div className="sk-cube sk-cube2"></div>
        <div className="sk-cube sk-cube3"></div>
        <div className="sk-cube sk-cube4"></div>
        <div className="sk-cube sk-cube5"></div>
        <div className="sk-cube sk-cube6"></div>
        <div className="sk-cube sk-cube7"></div>
        <div className="sk-cube sk-cube8"></div>
        <div className="sk-cube sk-cube9"></div>
      </div> 
    );
  },

});

module.exports = Loader;
