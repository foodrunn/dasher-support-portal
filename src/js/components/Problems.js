import React, { Component } from 'react'
const ReactBsTable  = require('react-bootstrap-table');
const BootstrapTable = ReactBsTable.BootstrapTable;
const TableHeaderColumn = ReactBsTable.TableHeaderColumn;
const moment = require('moment-timezone');

class Problems extends Component {

  supportPath() {
    return App.supportApi() + 'v1/';
  }


  constructor(props) {
    super(props);
    
    this.state = {
      orders: [],
      runs: []
    };
  }

  componentWillMount() {
    this.fetchOrders();
    this.fetchRuns();
  }


  componentWillUpdate() {
    App.closeMessage();
  }


  //
  // Network fetch functions
  //
  fetchOrders() {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'dashboard/orders/problem',
      dataType: 'json',
      success: orders => {
        if (!orders) {
          App.warn('There was a problem getting problem orders.');
          return console.error('Error fetching orders.', orders);
        }
        this.setState({orders});
      },
      error: err => {
        App.warn('Oops, something went wrong getting the orders.'); // TODO
        console.error('Server Error (orders):', err);
      }
    });
  }


  fetchRuns() {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'dashboard/runs/onHold',
      dataType: 'json',
      success: runs => {
        if (!runs) {
          App.warn('There was a problem getting problem runs.');
          return console.error('Error fetching runs.', runs);
        }
        this.setState({runs});
      },
      error: err => {
        App.warn('Oops, something went wrong getting the runs.'); // TODO
        console.error('Server Error (runs):', err);
      }
    });
  }


  renderDate(dateTime, row) {
    let date = moment(new Date(dateTime)); // Local TZ
    return date.format('MMM DD, YYYY hh:mm');
  }


  renderVerifyCode(code) {
    return <span className={ (code == 0 ? 'disabled' : '') }>{code}</span>;
  }


  render() {

    return ( <section className = "problems">
      <h3>Runs on Hold</h3>
      <BootstrapTable data={this.state.runs} striped hover>
        <TableHeaderColumn dataField='run_id' isKey={true} width="110px" >Run ID</TableHeaderColumn>
        <TableHeaderColumn dataField='created_date' dataFormat={this.renderDate}>Created Date</TableHeaderColumn>
        <TableHeaderColumn dataField='order_verification_code' dataFormat={this.renderVerifyCode}>Order Verification Code</TableHeaderColumn>
        <TableHeaderColumn dataField='hold_reason'>Hold Reason</TableHeaderColumn>
      </BootstrapTable>

      <h3>Problem Orders</h3>
      <BootstrapTable data={this.state.orders} striped hover>
        <TableHeaderColumn dataField='order_id' isKey={true} width="110px" >Order ID</TableHeaderColumn>
        <TableHeaderColumn dataField='run_id' width="110px" >Run ID</TableHeaderColumn>
        <TableHeaderColumn dataField='order_placed_time' dataFormat={this.renderDate}>Order Date</TableHeaderColumn>
        <TableHeaderColumn dataField='problem_description'>Problem Description</TableHeaderColumn>
      </BootstrapTable>
    </section>);
  }
}

module.exports = Problems;
