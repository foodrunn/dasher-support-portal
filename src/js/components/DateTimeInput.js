import React, { Component } from 'react'
import DateTime from 'react-datetime'

class TextInput extends Component {

  render() {
    let upload = (<div className="_no-upload" />);
    if (this.props.isUpload) {
      upload = this.renderUpload();
    }
    const fieldName = this.props.fieldName;
    const label = this.props.label || fieldName.charAt(0).toUpperCase() + fieldName.slice(1).replace(/_/g, ' ');
    
    return (
      <span className="labeled-input">
        <label className={this.props.required ? " required" : ""}>{ label }</label>
        <DateTime 
          className={"output "+fieldName}
          dateFormat="YYYY-MM-DD"
          timeFormat="HH:mm"
          onBlur={ dt => {
          	// dt is a moment object if valid datetime, else string
          	if (typeof dt === 'string' && dt !== '') {
              return this.props.onInvalid(true);
          	}
            this.props.onInvalid(false);
          }}
          onChange={ dt => {
          	this.props.handleChange(fieldName, dt);
          }}
          value={this.props.value}
          name={fieldName} />
      </span>
    );
  }
}

module.exports = TextInput;
