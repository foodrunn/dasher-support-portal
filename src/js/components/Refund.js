var React = require('react');

var Refund = React.createClass({
  basePath: function() {
    return App.api() + 'v1/run/';
  },

  componentWillUpdate: function() {
    // App.closeMessage();
  },

  getInitialState: function() {
    return this.resetState();
  },

  resetState: function() {
    return {
      action: 'OnlyRefundBuyer', // Match function name
      runId: '',
      dasherId: App.user.dasher_user_id , // Default to Admin ID
      orderId: '',
      runnerMsg: '',
      buyerMsg: '',
      description: '',
      refundAmount: 0,
      refundRunnerAmount: 0,
      partial: false
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    var data = this.buildFetchData();
    if (data === null) {
      return;
    }
    console.log('Fetch: ', this.basePath() + apiPath);
    console.log(data);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'PUT',
      contentType: 'application/json;charset=utf-8',
      // dataType: 'json',
      data: JSON.stringify(data),
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Refund succeeded');
      $("section.refunds select")[0].selectedIndex = 0;
      $('section.refunds input').val('');
      this.setState( this.resetState() );
    }
    console.log(data);
  },

  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Refund failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Refund failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Refund request failed.");
      console.log(err);
    }
  },

  buildFetchData: function() {
    if (!this.state.runId) {
      App.warn('You must enter a Run ID.');
      return null;
    }
    if (!this.state.orderId) {
      App.warn('You must enter an Order ID.');
      return null;
    }
    if (!this.state.description) {
      App.warn('You must enter a description for internal Joyrun use.');
      return null;
    }

    var data = {
      action_taken: this.state.action.replace(/^Partial/, ''),
      dasher_user_id: this.state.dasherId,
      description: this.state.description,
      params: {
        runner_message: this.state.runnerMsg,
        buyer_message: this.state.buyerMsg
      }
    };
    if (this.state.partial) {
      data.params.refund_amount = Number(this.state.refundAmount);

      if (this.state.action === 'PartialRefundBuyerFromRunner') {
        data.params.runner_contribution = Number(this.state.refundRunnerAmount);
      }
      if (!data.params.refund_amount) {
        App.warn('You must enter a refund amount.');
        return null;
      }
    }
    return data;
  },


  //
  // Form update funcitons
  //
  updateRunId: function(evt) {
    this.setState({
      runId: evt.target.value
    });
  },

  updateOrderId: function(evt) {
    this.setState({
      orderId: evt.target.value
    });
  },

  updateDesc: function(evt) {
    this.setState({
      description: evt.target.value
    });
  },

  updateRunMsg: function(evt) {
    this.setState({
      runnerMsg: evt.target.value
    });
  },

  updateBuyMsg: function(evt) {
    this.setState({
      buyerMsg: evt.target.value
    });
  },

  updateRefundAmt: function(evt) {
    this.setState({
      refundAmount: evt.target.value
    });
  },

  updateRefundRunnerAmt: function(evt) {
    this.setState({
      refundRunnerAmount: evt.target.value
    });
  },


  setAction: function(evt) {
    var actionFunciton = evt.target.value
    var partial = actionFunciton.match('Partial') != null;
    this.setState({
      action: actionFunciton,
      partial: partial
    });
  },


  issueRefund: function() {
    this[this.state.action]();
  },


  render: function() {

    return ( <section className="refunds">
      <h1>Issue a refund</h1>
      <p>You can issue full or partial refunds using the form below</p>
      <div className="form">
        <div>
          <label>Choose a refund type:</label>
          <div className="select">
            <select onChange={this.setAction}>
              <option value="OnlyRefundBuyer">Full refund to buyer</option>
              <option value="RefundBuyerFromRunner">Full refund to buyer from runner</option>
              <option value="PartialOnlyRefundBuyer">Partial refund to buyer</option>
              <option value="PartialRefundBuyerFromRunner">Partial refund to buyer from runner</option>
            </select>
          </div>
        </div>

        <div className="column">
          <label>Run ID:<span className="req">*</span></label>
          <input type="text" name="run-id" onChange={this.updateRunId} />
        </div>

        <div className="column">
          <label>Order ID:<span className="req"> * </span></label>
          <input type="text"
            name="order-id"
            onChange = {
              this.updateOrderId
            } />
        </div>

        <div>
          <label>Refund Description(<i>internal use only</i>):<span className="req">*</span></label>
          <input type="text" className="wide" name="description" onChange={this.updateDesc} />
        </div>

        <div>
          <label>Notification Message to Runner:</label>
            <input type="text"
            className="wide"
            name="runner-message"
            onChange = {
              this.updateRunMsg
            } />
        </div>

        <div>
          <label>Notification Message to Buyer:</label>
          <input type="text" className="wide" name="buyer-message" onChange={this.updateBuyMsg} />
        </div>

        <div className={ this.state.partial ? '' : 'hidden' }>
          <label>Refund Amount:<span className="req">*</span></label>
          $<input 
            type="text"
            name="refund-amount"
            type="number"
            min="0"
            step="0.01"
            onChange={this.updateRefundAmt} />
            
          <div className={ this.state.action === 'PartialRefundBuyerFromRunner' ? '' : 'hidden' }>
            <label>Amount Taken from Runner:</label>
              $<input 
                type="text"
                name="runner-contribution"
                type="number"
                min="0"
                step="0.01"
                value={ this.state.refundRunnerAmount }
                onChange={this.updateRefundRunnerAmt} />
          </div>
        </div>

        <div className="button" onClick={this.issueRefund}>Refund</div>
      </div>
    </section> )},


  //
  // Available actions (function name matches request param excluding "Partial")
  //
  OnlyRefundBuyer: function() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },


  RefundBuyerFromRunner: function() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },


  PartialOnlyRefundBuyer: function() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },


  PartialRefundBuyerFromRunner: function() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },
});

module.exports = Refund;
