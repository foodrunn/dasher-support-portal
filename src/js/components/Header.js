var React = require('react');
var Loader = require('./Loader');
import { Link } from 'react-router';

var Header = React.createClass({
  
  getInitialState: function() {
    return {
      mobileActive: false
    }  
  },


  renderOption: function(option) {
    var that = this;
    return (
      <Link key={option.path}
          to={option.path}
          activeClassName="active">
        {option.name.toUpperCase()}
      </Link>
    );
  },


  render: function() {
    var that = this;
    var options = [];
    options = this.props.menuOptions.map(this.renderOption);

    return (
      <div>
        <header className={ that.state.mobileActive ? 'mobile-active' : '' }>
          <div className="menu-options">
            <a onClick={ ()=>{location.href='/';} }>{'< Back'}</a>
            {options}
            <a onClick={ ()=>{location.href='/logout';} }>{'Sign Out'}</a>
          </div>
          <div className="env-right">
            <Loader />
          </div>
          <div className="env-right">
            [{ App.environment }]
          </div>
        </header>

        { this.props.children }
      </div>
    )
  }
});

module.exports = Header;
