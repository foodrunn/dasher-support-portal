import React, { Component } from 'react'
import ReactDOM from 'react-dom'


class UserOrders extends Component {

  constructor(props) {
    super(props);

    this.state = {
      orders: []
    };


    // Bind Methods
    this.fetchSuccess = this.fetchSuccess.bind(this);
    this.renderOrder = this.renderOrder.bind(this);
  }


  basePath() {
    return App.supportApi() + 'v1/';
  }


  componentWillMount() {
    if (this.props.userId) {
      this.getOrders(this.props.userId);
    }
  }


  componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      this.getOrders(nextProps.userId);
    }
  }
  

  componentDidUpdate(prevProps, prevState) {

    if (this.props.userId && this.state.orders.length !== prevState.orders.length) {

      // Scroll to the table
      let orderTable = ReactDOM.findDOMNode(this);
      $('html, body').clearQueue().animate({
          scrollLeft: '0px',
          scrollTop: $(orderTable).offset().top + 'px'
      }, 'fast');
    }
  }


  //
  // Network fetch functions
  //
  fetch(apiPath, userId) {
    const path = this.basePath() + apiPath + '?user_id=' + encodeURIComponent(userId);
    console.log('Fetch: ', path);
    var that = this;
    $.ajax({
      url: path,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  }


  fetchSuccess(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      this.setState({orders: data});
    }
  }


  fetchError(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Order Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Order search failed.");
      console.log(err);
    }
  }


  renderOrder(order) {
    var that = this;
    return (
      <tr key={ order.order_id }>
        <td>{ this.props.runLink(order.run_id) }</td>
        <td>{ this.props.orderLink(order.order_id) }</td>
        <td>{ order.restaurant_name }</td>
        <td>{ order.order_placed }</td>
        <td>{ order.order_status }</td>
        <td>{ order.status_reason || '-' }</td>
        <td>${ order.order_value.toFixed(2) }</td>
        <td>${ order.gas_money.toFixed(2) }</td>
        <td>${ order.service_fees.toFixed(2) }</td>
        <td>${ order.tax_total.toFixed(2) }</td>
        <td>${ order.refund_amount.toFixed(2) }</td>
        <td>${ order.total__price_discounted.toFixed(2) }</td>
      </tr>
    );
  }


  render() {
    var orderRows = this.state.orders.map(this.renderOrder);

    if (orderRows.length && this.props.userId) {
      return ( <div className="user-orders">
        <h3>{ this.props.displayName } Orders:</h3>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>Run ID</th>
              <th>Order ID</th>
              <th>Restaurant</th>
              <th>Order Placed</th>
              <th>Status</th>
              <th>Status Reason</th>
              <th>Order Value</th>
              <th>Gas Money</th>
              <th>Service Fees</th>
              <th>Tax</th>
              <th>Refund Amount</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
            { orderRows }
          </tbody>
        </table>
      </div>);
    }
    else if (this.props.userId) {
      return (<i>No Orders Found</i>);
    }
    else {
      return (<div className="user-orders"></div>);
    }
  }


  getOrders(userId) {
    if (!userId) {
      return;
    }
    var path = 'dashboard/orders';
    this.fetch(path, userId);
  }
}

module.exports = UserOrders;
