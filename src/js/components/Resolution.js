var React = require('react');

var Resolution = React.createClass({
  basePath: function() {
    return App.api() + 'v1/run/';
  },

  componentWillUpdate: function() {
    App.closeMessage();
  },

  getInitialState: function() {
    return this.resetState();
  },

  resetState: function() {
    return {
      action: 'ResolveProblem', // Match function name
      runId: '',
      dasherId: App.user.dasher_user_id || '' , // Default to Admin ID
      orderId: '',
      description: ''
    };
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    var data = this.buildFetchData();
    if (data === null) {
      return;
    }
    console.log('Fetch: ', this.basePath() + apiPath);
    console.log(data);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'PUT',
      contentType: 'application/json;charset=utf-8',
      // dataType: 'json',
      data: JSON.stringify(data),
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },

  fetchSuccess: function(data) {
    if (data && data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Resolution succeeded');
      $('section.resolutions input').val('');
    }
    console.log(data);
  },

  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Resolution failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Resolution failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Resolution request failed.");
      console.log(err);
    }
  },

  buildFetchData: function() {
    if (!this.state.runId) {
      App.warn('You must enter a Run ID.');
      return null;
    }
    if (this.state.action !== 'ReleaseHold' && !this.state.orderId) {
      App.warn('You must enter a Order ID.');
      return null;
    }
    if (!this.state.description) {
      App.warn('You must enter a Description for internal Joyrun use.');
      return null;
    }

    var data = {
      action_taken: this.state.action,
      dasher_user_id: this.state.dasherId,
      description: this.state.description
    };

    if (this.state.action === 'ReleaseHold') {
      data.params = {
        runner_message: this.state.runnerMsg,
        buyer_message: this.state.buyerMsg
      }
    }
    return data;
  },


  //
  // Form update funcitons
  //
  updateRunId: function(evt) {
    this.setState({
      runId: evt.target.value
    });
  },

  updateOrderId: function(evt) {
    this.setState({
      orderId: evt.target.value
    });
  },

  updateDesc: function(evt) {
    this.setState({
      description: evt.target.value
    });
  },

  updateRunMsg: function(evt) {
    this.setState({
      runnerMsg: evt.target.value
    });
  },

  updateBuyMsg: function(evt) {
    this.setState({
      buyerMsg: evt.target.value
    });
  },


  setAction: function(evt) {
    var actionFunciton = evt.target.value
    this.setState({
      action: actionFunciton,
    });

    // Use the admin ID
    // if (actionFunciton !== 'ReleaseHold') {
    //   this.setState({
    //     dasherId: App.user.dasher_user_id
    //   });
    // }
  },


  resolveIssue: function() {
    this[this.state.action]();
  },


  render: function() {
    var releaseHold = this.state.action === 'ReleaseHold';

    return ( <section className="resolutions">
      <h1>Resolve a Problem</h1>
      <p>You can mark a problem as resolved or release a hold using the form below</p>
      <div className="form">
        <div>
          <label>Choose a resolution type:</label>
          <div className="select">
            <select onChange={this.setAction}>
              <option value="ResolveProblem">Resolve a Problem</option>
              <option value="ReleaseHold">Remove Hold</option>
            </select>
          </div>
        </div>

        <div>
          <label>Run ID: <span className="req"> * </span></label>
          <input type="text"
            name="run-id"
            onChange = {
              this.updateRunId
            } />
        </div>

        <div className={ releaseHold ? 'hidden' : '' }>
          <label>Order ID: <span className="req"> * </span></label>
          <input type="text"
            name="order-id"
            onChange = {
              this.updateOrderId
            } />
        </div>

        <div>
          <label>Resolution Description(<i>internal use only</i>):<span className="req">*</span></label>
          <input type="text" className="wide" name="description" onChange={this.updateDesc} />
        </div>

        <div className={ releaseHold ? '' : 'hidden' }>
          <label>Notification Message to Runner:</label>
            <input type="text"
              className="wide"
              name="runner-message"
              onChange = {
                this.updateRunMsg
              } />
        </div>

        <div className={ releaseHold ? '' : 'hidden' }>
          <label>Notification Message to Buyer:</label>
          <input type="text" className="wide" name="buyer-message" onChange={this.updateBuyMsg} />
        </div>

        <div className="button" onClick={this.resolveIssue}>Resolve Issue</div>
      </div>
    </section> )},


  //
  // Available actions
  //
  ResolveProblem: function() {
    var path = this.state.runId + '/order/' + this.state.orderId + '/action';
    this.fetch(path);
  },


  ReleaseHold: function() {
    var path = this.state.runId + '/action';
    this.fetch(path);
  }
});

module.exports = Resolution;
    