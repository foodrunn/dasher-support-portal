import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';

class RegionMultiSelect extends Component {

  constructor(props) {
    super(props);

    this.state = {
      regionSearch: '' // Text value in the autocomplete input
    };
    
    // Bind methods
    this.renderRegion = this.renderRegion.bind(this);
    this.changeRegionSearch = this.changeRegionSearch.bind(this);
    this.selectRegion = this.selectRegion.bind(this);
    this.removeRegion = this.removeRegion.bind(this);
  }


  componentWillUpdate(newProps, newState) {

    // // Allow populating with initial value
    // if (newProps.value && newProps.value === this.props.value && newProps.allRegions.length && !this.state.regionSearch) {
    //   const region = newProps.allRegions.find( r => r.region_id == this.props.value );
    //   this.setState({ regionSearch: region.name || region.official_name});
    // }
    
    // Allow a reset
    if (newProps.initialRegion === null && this.state.regionSearch !== '') {
      this.setState({regionSearch: ''});
    }
  }


  renderRegion(rid, i) {
    const region = this.props.allRegions.find(r => r.region_id == rid);
    if (region) {
      return <div key={i} className="region pill" onClick={ () => this.removeRegion(region) }>
        { this.getRegionName(region) }
      </div>;
    }
  }


  render() {
    return (
      <div className="auto-select region-select">
        <label>{ this.props.label }</label>
        <Autocomplete
          value={this.state.regionSearch}
          items={this.props.allRegions}
          inputProps={{
            placeholder: 'Start typing...'
          }}
          getItemValue={(region) => this.getRegionName(region)}
          shouldItemRender={this.matchAutocomplete}
          onChange={this.changeRegionSearch}
          onSelect={this.selectRegion}
          renderItem={(region, isHighlighted) => (
            <div key={region.region_id} className={isHighlighted ? 'autocomplete-highlighted' : 'autocomplete-item'}>
              {this.getRegionName(region)}
            </div>
        )} />
        <div className="region-pills">
          { this.props.value.map(this.renderRegion) }
          { !this.props.value.length ? <div className="pill">ALL</div> : '' }
        </div>
      </div>
    );
  }


  matchAutocomplete(item, value) {
    var name = item.official_name || item.name;
    return name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
  }


  // Updating the text in the autocomplete input
  changeRegionSearch(evt, val) {
    if (val) {
      this.setState({ regionSearch: val });
    }
    // Value was deleted, treat this like an 'onSelect'
    else {
      this.setState({ regionSearch: '' });
    }
  }


  selectRegion(regionName, region) {
    this.setState({ regionSearch: '' })
    this.props.onSetRegion(region);
  }


  removeRegion(region) {
    this.props.onRemoveRegion(region);
  }


  getRegionName(r) {
    return r.official_name || r.name;
  }
}

module.exports = RegionMultiSelect;
