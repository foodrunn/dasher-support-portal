var React = require('react');
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var hashHistory = require('react-router').hashHistory;
var Header = require('./Header');
var Messaging = require('./Messaging');
var Refund = require('./Refund');
var Resolution = require('./Resolution');
var Users = require('./Users');
var Promotion = require('./Promotion');
var Problems = require('./Problems');
var PromoCodes = require('./codes/PromoCodes');

var NavWrapper = React.createClass({

	menuOptions: [
    {
    	name: 'users',
    	path: '/users'
    },
    {
    	name: 'refunds',
    	path: '/refunds'
    },
    {
    	name: 'resolution',
    	path: '/resolution'
    },
    {
    	name: 'promotions',
    	path: '/promotions'
    },
    {
      name: 'problems',
      path: '/problems'
    },
    {
      name: 'promo codes',
      path: '/codes'
    }
  ],


  changeView: function(newPath) {
  	hashHistory.push(newPath);
  },

  render: function() {
  	var that = this;
  	var HeaderWrapper = React.createClass({
			render: function() {
			  return <div>
	  			<Header menuOptions={ that.menuOptions } changeView={ that.changeView } children={ this.props.children } />
		  		<Messaging />
		  	</div>
			}
		});

    return (
      <div className="wrapper">
    		<Router history={hashHistory}>
    			<Route component={HeaderWrapper}>

            <Route path="/" component={HomeRdeirect} />

            <Route path="/users" component={Users}>
              <Route path="/users(/:searchParams)(/orders/:orderUserId/:orderUserDisplay)" component={Users} />
              <Route path="/users(/:searchParams)(/runs/:runUserId/:runUserDisplay)" component={Users} />
            </Route>

	        	<Route path="/refunds" component={Refund} />
	        	<Route path="/resolution" component={Resolution} />
            <Route path="/promotions" component={Promotion} />
            <Route path="/problems" component={Problems} />
	        	<Route path="/codes" component={PromoCodes}>
              <Route path=":tab" component={PromoCodes}>
                <Route path=":timezone" component={PromoCodes}>
                  <Route path=":code" component={PromoCodes}>
                    <Route path=":region_ids" component={PromoCodes}>
                      <Route path=":start_date" component={PromoCodes}>
                        <Route path=":end_date" component={PromoCodes} />
                      </Route>
                    </Route>
                  </Route>
                </Route>
              </Route>
            </Route>
	        </Route>
        </Router>
      </div>
    );
  }
});



var HomeRdeirect = React.createClass({
  render: function() {
    window.location.hash = '/users';
    return <div />;
  }
});

module.exports = NavWrapper;
