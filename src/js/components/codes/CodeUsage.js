import React, { Component } from 'react'
const moment = require('moment-timezone');
import DateTimeInput from '../DateTimeInput'
import RegionMultiSelect from '../RegionMultiSelect'
import UsageTable from './UsageTable'

class CodeUsage extends Component {

  clientPath() {
    return App.api() + 'v1/';
  }

  supportPath() {
    return App.supportApi() + 'v1/';
  }


  constructor(props) {
    super(props);
    
    this.state = {
      allRegions: [],
      usage: [],
      activeCode: '',
      invalidDate: false
    };

    // Set the initial timezone
    moment.tz.setDefault(this.props.params.timezone);

    // Bind methods
    this.setTimezone = this.setTimezone.bind(this);
    this.setCode = this.setCode.bind(this);
    this.setRegion = this.setRegion.bind(this);
    this.removeRegion = this.removeRegion.bind(this);
    this.setDate = this.setDate.bind(this);
    this.handleInvalidDate = this.handleInvalidDate.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }


  componentWillMount() {
    this.fetchRegions();
  }


  componentWillUpdate(nextProps, nextState) {

    // Set the chosen timezone
    if (this.props.params.timezone !== nextProps.params.timezone) {
      moment.tz.setDefault(nextProps.params.timezone.replace('|', '/'));
    }
  }


  //
  // Network fetch functions
  //
  fetchUsage(data) {
    $.ajax({
      url: this.supportPath() + 'metrics/promoPrograms/couponCodes/usage',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: usage => {
        if (!usage) {
          App.warn('Unable to load usage at this time.');
          return console.error('No usage', usage);
        }
        this.setState({
          usage: usage.region_usage,
          activeCode: usage.coupon_code
        });
      },
      error: err => {
        const msg = err && err.responseJSON ? err.responseJSON.message : 'Something went wrong';
        App.warn(msg);
        console.error('Error fetching usage:', err);
      }
    });
  }


  fetchRegions() {
    $.ajax({
      url: this.clientPath() + 'app/config',
      dataType: 'json',
      success: data => {
        const allRegions = data.regions;
        if (!allRegions) {
          App.warn('Region fetch error.');
          return console.error('No regions!', data);
        }
        this.setState({ allRegions });
      },
      error: err => {
        console.error('Region fetch failed:', err);
        App.warn('Unable to fetch regions.');
      }
    });
  }


  render() {
    let params = this.props.params;
    Object.keys(params).map(function(key, index) {
      let val = decodeURIComponent(params[key]);
      params[key] = val === '+' ? '' : val;
    });
    const timezone = params.timezone.replace('|', '/');
    const regionIds = params.region_ids ? params.region_ids.split(',') : [];

    return <div className="code-usage">
      <div className="form">
        <div className="column triple">
          <label>Enter Code:</label>
          <input
            type="text"
            value={ params.code || '' }
            onChange={ this.setCode } />
        </div>

        <div className="column triple">
          <label>Choose Region</label>
          <RegionMultiSelect
            value={ regionIds }
            allRegions={ this.state.allRegions }
            onRemoveRegion={ this.removeRegion }
            onSetRegion={ this.setRegion } />
        </div>

        <br />

        <div className="column triple">
          <DateTimeInput 
            label="Start Date"
            fieldName="start_date"
            value={ params.start_date }
            handleChange={this.setDate}
            onInvalid={ this.handleInvalidDate } />
        </div>

        <div className="column triple">
          <DateTimeInput 
            label="End Date"
            fieldName="end_date"
            value={ params.end_date }
            handleChange={this.setDate}
            onInvalid={ this.handleInvalidDate } />
        </div>

        <div className="column triple">
          <span className="labeled-input">
            <label>Time Zone</label>
            <select value={ timezone } onChange={ this.setTimezone }>
              { App.usTimeZones.map((tz,i) => <option key={i} value={tz.code}>{tz.label}</option>) }
            </select>
          </span>
        </div>

        <div className={ 'button submit' + (this.isFormComplete() ? '' : ' disabled') }
            onClick={ this.submitForm }>
          SEARCH
        </div>
      </div>
      <br style={{clear:'both'}}/>
      <h3>{ this.state.activeCode }</h3>
      <UsageTable usage={ this.state.usage } />
    </div>;
  }


  setCode(evt) {
    const params = this.props.params;
    const timezone = params.timezone;
    const code = evt.target.value;
    const regions = params.region_ids;
    const start = params.start_date;
    const end = params.end_date;
    this.updateHash(timezone, code, regions, start, end);
  }


  setRegion(region) {
    const params = this.props.params;
    const timezone = params.timezone;
    const code = params.code;
    const start = params.start_date;
    const end = params.end_date;

    let regionIds = params.region_ids ? params.region_ids.split(',') : [];
    if (regionIds.indexOf(region.region_id.toString()) >= 0) {
      return App.warn('Region already selected!');
    }
    regionIds.push(region.region_id);
    regionIds = regionIds.join(',');
    this.updateHash(timezone, code, regionIds, start, end);
  }


  removeRegion(region) {
    const params = this.props.params;
    const timezone = params.timezone;
    const code = params.code;
    const start = params.start_date;
    const end = params.end_date;

    let regionIds = params.region_ids ? params.region_ids.split(',') : [];
    regionIds.splice(regionIds.indexOf(region.region_id.toString()), 1);
    regionIds = regionIds.join(',');
    this.updateHash(timezone, code, regionIds, start, end);
  }


  setTimezone(evt) {
    const params = this.props.params;
    const timezone = evt.target.value.replace('/', '|');
    const code = params.code;
    const regions = params.region_ids;
    const start = params.start_date;
    const end = params.end_date;
    this.updateHash(timezone, code, regions, start, end);
  }


  setDate(key, val) {
    // A valid date comes back as a moment object
    if (typeof val !== 'string') {
      const params = this.props.params;
      if (params.start_date && params.end_date) {

        if (key === 'start_date' && moment(val).isAfter(moment(params.end_date))) {
          return App.warn('Start date cannot be after end date.');
        }
        if (key === 'end_date' && moment(val).isBefore(moment(params.start_date))) {
          return App.warn('End date cannot be before end date.');
        }
      }

      val = val.format('YYYY-MM-DD HH:mm');
    }
    const params = this.props.params;
    const timezone = params.timezone;
    const code = params.code;
    const regions = params.region_ids;
    let start = params.start_date;
    let end = params.end_date;

    if (key === 'start_date'){
      start = val;
    }
    else if (key === 'end_date'){
      end = val;
    }
    this.updateHash(timezone, code, regions, start, end);
  }


  handleInvalidDate(invalid) {
    this.setState({ invalidDate: invalid });
  }


  updateHash(timezone, code, regions, start, end) {
    timezone = encodeURIComponent(timezone || '+');
    code = encodeURIComponent(code || '+');
    regions = encodeURIComponent(regions || '+');
    start = encodeURIComponent(start || '+');
    end = encodeURIComponent(end || '+');

    let hash = '/codes/usage/' + timezone;
    if (code) {
      hash += '/' + code;
    }
    if (regions) {
      hash += '/' + regions;
    }
    if (start) {
      hash += '/' + start;
    }
    if (end) {
      hash += '/' + end;
    }
    location.hash = hash;
  }


  isFormComplete() { // Dates are optional, no regions means "all"
    const params = this.props.params;
    return params.code && !this.state.invalidDate;
  }


  submitForm() {
    const params = this.props.params;
    let regions = params.region_ids.split(',');
    if (regions[0] === '') {
      regions = [];
    }
    else {
      regions = regions.map( r => Number(r) );
    }

    const data = {
      coupon_code: params.code,
      region_ids: regions,
      start_datetime: '',
      end_datetime: ''
    }

    // Convert from chosen timezone to UTC before sending
    if (params.start_date) {
      const start = moment(params.start_date).utc();
      console.log('UTC start:', start.format('YYYY-MM-DD HH:mm:ssZ'));
      data.start_datetime = start.format('YYYY-MM-DD HH:mm');
    }
    if (params.end_date) {
      const end = moment(params.end_date).utc();
      console.log('UTC start:', end.format('YYYY-MM-DD HH:mm:ssZ'));
      data.end_datetime = end.format('YYYY-MM-DD HH:mm');
    }

    this.fetchUsage(data);
  }
}

module.exports = CodeUsage;
