import React, { Component } from 'react'
const ReactBsTable  = require('react-bootstrap-table');
const BootstrapTable = ReactBsTable.BootstrapTable;
const TableHeaderColumn = ReactBsTable.TableHeaderColumn;

class UsageTable extends Component {

  render() {
    const regexFilter = {type: 'RegexFilter', delay: 600};

    return <div className="usage-table">
      <BootstrapTable data={this.props.usage} striped hover>
        <TableHeaderColumn isKey dataField='region_name' filter={ regexFilter } > Region </TableHeaderColumn>
        <TableHeaderColumn dataField='applied_count' dataSort > Applied </TableHeaderColumn>
        <TableHeaderColumn dataField='redeemed_count' dataSort > Redeemed </TableHeaderColumn>
      </BootstrapTable>
    </div>;
  }
}

module.exports = UsageTable;
