// https://foodrunn.atlassian.net/wiki/spaces/PROD/pages/101679105/Promo+Code+API

import React, { Component } from 'react'
import moment from 'moment-timezone'
import CodeForm from './CodeForm'
import CodeUsage from './CodeUsage'

class PromoCodes extends Component {


  constructor(props) {
    super(props);

    this.defaultTimezone = App.usTimeZones[0].code;
  }


  renderTabs() {
    const tab = this.props.params.tab || '';

    return <div className="tabs">
      <div className={ 'tab' + (tab === '' ? ' active' : '') } 
          onClick={ () => location.hash = '/codes' }>
        Create/Delete Code
      </div>
      <div className={ 'tab' + (tab === 'usage' ? ' active' : '') } 
          onClick={ () => location.hash = '/codes/usage/' + this.defaultTimezone.replace('/', '|') }>
        View Code Usage
      </div>
    </div>;
  }


  renderForm() {
    return <CodeForm />;
  }


  renderUsage() {
    return <CodeUsage params={ this.props.params } />;
  }


  render() {
    const tab = this.props.params.tab || '';

    return <section className="promo-codes">
      { this.renderTabs() }
      { tab === '' ? this.renderForm() : this.renderUsage() }
    </section>;
  }
}

module.exports = PromoCodes;
