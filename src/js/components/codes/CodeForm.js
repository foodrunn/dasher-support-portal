import React, { Component } from 'react'
import moment from 'moment-timezone'
import DateTimeInput from '../DateTimeInput'

class CodeForm extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      programs: [],
      program: {},
      timezone: App.usTimeZones[0].code,
      form: {
        end_datetime: '',
        coupon_code: ''
      },
      newCode: '',
      deleteCode: ''
    };

    // Set the initial timezone
    moment.tz.setDefault(this.state.timezone);

    // Bind methods
    this.setProgram = this.setProgram.bind(this);
    this.setDate = this.setDate.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }


  supportPath() {
    return App.supportApi() + 'v1/';
  }


  componentWillMount() {
    this.fetchPrograms()
  }


  // Coupon code in the form should always be one of the two
  componentWillUpdate(nextProps, nextState) {

    // Set the chosen timezone
    if (this.state.timezone !== nextState.timezone) {
      moment.tz.setDefault(nextState.timezone);
    }

    const form = $.extend({}, this.state.form);
    if (nextState.newCode !== this.state.newCode) {
      form.coupon_code = nextState.newCode;
      this.setState({ form });
    }
    else if (nextState.deleteCode !== this.state.deleteCode) {
      form.coupon_code = nextState.deleteCode;
      this.setState({ form });
    }
  }


  //
  // Network fetch functions
  //
  fetchPrograms() {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'metrics/promoPrograms',
      dataType: 'json',
      success: programs => {
        if (!programs) {
          App.warn('Unable to load programs at this time.');
          return console.error('No programs', programs);
        }
        this.setState({ programs });
      },
      error: err => {
        const msg = err && err.responseJSON ? err.responseJSON.message : 'Something went wrong';
        App.warn(msg);
        console.error('Error fetching programs:', err);
      }
    });
  }


  fetchProgram(pid) {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'metrics/promoPrograms/' + pid,
      dataType: 'json',
      success: program => {
        if (!program) {
          App.warn('Unable to load program at this time.');
          return console.error('No program', program);
        }
        this.setState({ program, newCode: '', deleteCode: '' });
      },
      error: err => {
        const msg = err && err.responseJSON ? err.responseJSON.message : 'Something went wrong';
        App.warn(msg);
        console.error('Error fetching program:', err);
      }
    });
  }


  createCode(pid, data) {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'metrics/promoPrograms/' + pid + '/couponCodes',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: resp => {
        App.notify('Code created successfully.');
        this.fetchProgram(pid);
      },
      error: err => {
        const msg = err && err.responseJSON ? err.responseJSON.message : 'Something went wrong';
        App.warn(msg);
        console.error('Error adding code:', err);
      }
    });
  }


  deleteCode(pid, data) {
    var that = this;
    $.ajax({
      url: this.supportPath() + 'metrics/promoPrograms/' + pid + '/couponCodes',
      type: 'DELETE',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: resp => {
        App.notify('Code deleted successfully.');
        this.fetchProgram(pid);
      },
      error: err => {
        const msg = err && err.responseJSON ? err.responseJSON.message : 'Something went wrong';
        App.warn(msg);
        console.error('Error deleting code:', err);
      }
    });
  }


  renderCodes(active) {
    if (!this.state.program.prog_id) {
      return <p>Choose a program</p>;
    }
    let codes = active ? this.state.program.valid_coupon_codes : this.state.program.invalid_coupon_codes;
    
    if (codes.length) {
      return codes.map( (c,i) => c ? <div key={i} className="pill code">{ c }</div> : '' );
    }
    else {
      return active ? <p>No active codes</p> : <p>No inactive codes</p>;
    }
  }


  render() {
    const program = this.state.program;
    const form = this.state.form;
    const adding = this.state.newCode !== '';
    const deleting = this.state.deleteCode !== '';

    return <div className="create-delete-form form">
      <div className="column">
        <label>Select Program</label>
        <select
            value={ program.prog_id }
            onChange={ this.setProgram }>
          <option value="">Choose Program</option>
          { this.state.programs.map((p,i) => <option key={i} value={p.prog_id}>{ p.name }</option>) }
        </select>
      </div>

      <div className="column">
        <label>Program Description</label>
        <div className="description">{ program.description || <p>No description yet</p> }</div>
      </div>

      <div className="column">
        <label>Program Active Codes</label>
        <br />
        { this.renderCodes(true) }
      </div>

      <div className="column">
        <label>Program Inactive Codes</label>
        <br />
        { this.renderCodes(false) }
      </div>

      <div className={ 'highlight' + (this.state.program.prog_id ? '' : ' disabled') }>

        <div className="column">
          <label>Code to Create/Update</label>
          <input
            type="text"
            disabled={ deleting }
            value={ this.state.newCode }
            onChange={ evt => this.setState({newCode: evt.target.value}) } />
        </div>

        <div className="column double">
          <div>
            <DateTimeInput 
              label="Validity End Date"
              fieldName="end_datetime"
              value={ form.end_datetime }
              handleChange={ this.setDate }
              onInvalid={ this.handleInvalidDate } />
              <p>optional</p>
          </div>
          <div>
            <span className="labeled-input">
              <label>Time Zone</label>
              <select onChange={ evt => this.setState({ timezone: evt.target.value }) }>
                { App.usTimeZones.map((tz,i) => <option key={i} value={tz.code}>{tz.label}</option>) }
              </select>
            </span>
          </div>
        </div>

        <p className="note">Code will be active as soon as this form is submitted.</p>
        <hr />
        <div className="column label-inline">
          <label>Code to Delete</label>
          <input
            type="text"
            disabled={ adding }
            value={ this.state.deleteCode }
            onChange={ evt => this.setState({deleteCode: evt.target.value}) } />
        </div>

        <div className={ 'button submit' + (this.isFormComplete() ? '' : ' disabled') }
             onClick={ this.submitForm }>
          { adding ? 'CREATE' : deleting ? 'DELETE' : 'SUBMIT' }
        </div>
      </div>
    </div>;
  }


  setProgram(evt) {
    const prog_id = evt.target.value;
    
    if (prog_id !== '') {
      this.fetchProgram(prog_id);
    }
    else {
      this.setState({ program: {} });
    }
  }


  setDate(key, val) {
    
    // A valid date comes back as a moment object
    if (typeof val !== 'string') {
      const form = this.state.form;
      val = val.format('YYYY-MM-DD HH:mm');
    }
    this.updateForm(key, val);
  }


  handleInvalidDate(invalid) {
    if (invalid) {
      App.warn('Invalid date');
    }
  }


  isFormComplete() {
    const form = this.state.form;
    return form.coupon_code !== '';
  }


  updateForm(key, val) {
    const form = $.extend({}, this.state.form);
    form[key] = val;
    this.setState({ form });
  }


  submitForm() {
    const adding = this.state.newCode !== '';
    const deleting = this.state.deleteCode !== '';
    const pid = this.state.program.prog_id;
    const data = Object.assign({}, this.state.form);

    if (adding) {

      // Convert from chosen timezone to UTC before sending
      if (data.end_datetime) {
        const val = moment(data.end_datetime).utc();
        console.log('UTC:', val.format('YYYY-MM-DD HH:mm:ssZ'));
        data.end_datetime = val.format('YYYY-MM-DD HH:mm');
      }

      this.createCode(pid, data);
    }
    else if (deleting) {
      this.deleteCode(pid, {
        coupon_code: data.coupon_code
      });
    }
  }
}

module.exports = CodeForm;
