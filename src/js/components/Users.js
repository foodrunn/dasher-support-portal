import React from 'react'
import UserOrders from './UserOrders'
import UserRuns from './UserRuns'
import Modal from 'react-modal'

var Users = React.createClass({

  clientPath: function() {
    return App.api() + 'v1/';
  },


  supportPath: function() {
    return App.supportApi() + 'v1/';
  },


  // Pass the encoded JSON string
  initializeStateObject: function(defaults) {
    try {
      defaults = JSON.parse(decodeURIComponent(defaults));
    } catch (e) {
      console.warn('Bad params!', e);
    } finally {
      return {
        searchMode: defaults.searchMode || 'user',
        firstName: defaults.firstName || '',
        lastName: defaults.lastName || '',
        email: defaults.email || '',
        phone: defaults.phone || '',
        userId: defaults.userId || '',
        runId: defaults.runId || '',
        orderId: defaults.orderId || '',
        users: [],
        run: {},
        order: {},
        showVerifyModal: false,
        verifyCode: ''
      }
    }
  },


  getInitialState: function() {
    return this.initializeStateObject(this.props.params.searchParams || '{}');
  },


  componentWillUpdate: function(nextProps, nextState) {

    // Checking for URL changes (back button or manually typed)
    if (nextProps.params.searchParams !== this.props.params.searchParams) {
      this.setState(this.initializeStateObject(nextProps.params.searchParams || '{}'), () => {
        // Do a search
        this.getSubmitFunc().submitFunc();
      });
    }
  },


  componentDidMount: function() {
    $("input:text").focus(function() { $(this).select(); } );
    $('.focus-me').focus();

    // Do a search if the URL params changed
    if (!$.isEmptyObject(this.props.params.searchParams)) {
      this.getSubmitFunc().submitFunc();
    }
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    App.closeMessage();
    
    console.log('Fetch: ', this.supportPath() + apiPath);
    var that = this;
    $.ajax({
      url: this.supportPath() + apiPath,
      type: 'GET',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },


  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      var newState = {
        firstName: '', lastName: '', email: '', phone: '', userId: '', runId: '', orderId: '',
      };
      switch (this.state.searchMode) {
        case 'user':
          newState.users = data;
          if (data.length === 0) {
            App.warn('No users found for this search.')
          }
          break;
        case 'run':
          newState.run = data;
          break;
        case 'order':
          newState.order = data;
          break;
      }

      this.setState(newState);
      $("input:text").val('');
    }
    console.log(data);
  },


  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Search failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Search failed. Nothing found.");
      console.log(err);
    }
  },


  renderUser: function(user) {
    var that = this;
    var selected = '';
    if (this.props.params.orderUserId === user.dasher_user_id || this.props.params.runUserId === user.dasher_user_id) {
      selected = 'selected';
    }
    return (
      <tr className={ selected } key={ user.dasher_user_id }>
        <td>{ user.dasher_user_id }</td>
        <td>{ user.display_name }</td>
        <td>{ user.first_name }</td>
        <td>{ user.last_name }</td>
        <td>{ user.email_address || 'unknown' }</td>
        <td>{ user.phone_number || 'unknown' }</td>
        <td>{ user.region_name }</td>
        <td>{ user.secure_user_id }</td>
        <td className="link" onClick={ function() {
          let hash = location.hash.replace(/\/runs\/.*$/, '').replace(/\/orders\/.*$/, '');
          location.hash = hash+'/runs/'+encodeURI(user.secure_user_id)+'/'+encodeURI(user.display_name);
        }}>Runs</td>
        <td className="link" onClick={ () => {
          let hash = location.hash.replace(/\/orders\/.*$/, '').replace(/\/runs\/.*$/, '');
          location.hash = hash+'/orders/'+encodeURI(user.secure_user_id)+'/'+encodeURI(user.display_name);
        }}>Orders</td>
      </tr>
    );
  },


  renderUserTable: function() {
    var userRows = this.state.users.map(this.renderUser);

    if (!userRows.length) {
      return <div />;
    }

    return (
      <div>
        <h3>Found Users:</h3>
        <table cellSpacing="0">
          <thead>
            <tr>
              <th>User ID</th>
              <th>Display Name</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email Address</th>
              <th>Phone Number</th>
              <th>Region</th>
              <th>Hashed User ID</th>
              <th>Runs</th>
              <th>Orders</th>
            </tr>
          </thead>
          <tbody>
            { userRows }
          </tbody>
        </table>
      </div>);
  },


  renderRunLink: function(runId) {
    return <span className="link" onClick={ () => {this.setRunId({target: {value:runId}},true)} }>{runId}</span>;
  },


  renderOrderLink: function(orderId) {
    return <span className="link" onClick={ () => {this.setOrderId({target: {value:orderId}}, true)} }>{orderId}</span>;
  },


  renderVerifyButton: function(run) {
    if (run.flags.indexOf('invoiced') >= 0) {

      return <p><label>Verify Run: </label>
        <div className="button verify" onClick={ () => this.setState({showVerifyModal: true}) }>
          Verify
        </div>
      </p>;
    }
    else {
      return '';
    }
  },


  renderRun: function() {
    var that = this;
    var run = this.state.run;
    if (!run || $.isEmptyObject(run)) {
      return <div />;
    }

    var renderRunOrders = function(orderId) {
      return <span key={orderId}>{ that.renderOrderLink(orderId) },</span>;
    }

    if (!run.flags) run.flags = ['invoiced'];
    const verifyButton = this.renderVerifyButton(run);

    try {
      return (
        <div className="run-order-data">
          <h3>Run Details ({ run.runId })</h3>
          <p><label>Restaurant: </label>{ run.restaurantName }</p>
          <p><label>Region: </label>{ run.regionName }</p>
          <p><label>Created: </label>{ new Date(run.createdDate).toString() }</p>
          <p><label>Runner: </label>{ run.runnerName }</p>
          <p><label>Orders: </label>{ run.orderIdsForRun.map(renderRunOrders) }</p>
          <p><label>Earnings: </label>${ run.earnings.toFixed(2) }</p>
          <p><label>Reimbursed: </label>${ run.reimburseAmount.toFixed(2) }</p>
          <p><label>Payment Type: </label>{ run.paymentMethod }</p>
          <p><label>State: </label>{ run.state }</p>
          { verifyButton }
        </div>);
    } catch (err) {
      console.error("ERROR:", err);
      App.warn('Sorry, something went wrong.');
      return <div />;
    }
  },


  renderOrder: function() {
    var that = this;
    var order = this.state.order;
    if (!order || $.isEmptyObject(order)) {
      return <div />;
    }

    var renderOrderRun = function(runId) {
      return that.renderRunLink(runId);
    }

    var tip = order.totalTipAmount || null;
    return (
      <div className="run-order-data">
        <h3>Order Details ({ order.orderId })</h3>
        <p><label>Run ID: </label>{ renderOrderRun(order.runId) }</p>
        <p><label>Restaurant: </label>{ order.restaurantName }</p>
        <p><label>Buyer: </label>{ order.buyerName }</p>
        <p><label>Runner: </label>{ order.runnerName }</p>
        <p><label>Payment Type: </label>{ order.paymentMethod }</p>
        <p><label>Total Price: </label>${ order.totalPrice.toFixed(2) }</p>
        <p><label>Total Tip: </label>{ tip ? '$' + tip.toFixed(2) : 'NONE' }</p>
        <p><label>State: </label>{ order.orderState }</p>
        <p><label>Status Reason: </label>{ order.statusReason || 'NA' }</p>
      </div>);
  },


  render: function() {
    let userTable = this.renderUserTable();
    let runDetails = this.renderRun();
    let orderDetails = this.renderOrder();

    let submit = this.getSubmitFunc();

    return ( <section className="user-search">
      <h1>Search for Users</h1>
      <p>Use this form to search for user details and find runs and orders for the given user</p>

      <div className="form">

        <div className="column">
          <label>First Name:</label>
          <input value={this.state.firstName} onKeyUp={ this.detectEnter } type="text" onChange={ this.setFirstName } name="first-name" className="focus-me" />
        </div>
        <div className="column">
          <label>Last Name:</label>
          <input value={this.state.lastName} onKeyUp={ this.detectEnter } type="text" onChange={ this.setLastName } name="last-name" />
        </div>
        <br />

        <div className="column">
          <label>Email Address:</label>
          <input value={this.state.email} onKeyUp={ this.detectEnter } type="text" onChange={ this.setEmail } name="email" />
        </div>
        <div className="column">
          <label>Phone Number:</label>
          <input value={this.state.phone} onKeyUp={ this.detectEnter } type="text" onChange={ this.setPhone } name="phone" />
        </div>
        <br />

        <div className="column">
          <label>User ID:</label>
          <input value={this.state.userId} onKeyUp={ this.detectEnter } type="text" onChange={ this.setUserId } name="user-id" />
        </div>
        <br />

        <div className="column">
          <label>Run ID:</label>
          <input value={this.state.runId} onKeyUp={ this.detectEnter } type="number" onChange={ this.setRunId } name="run" />
        </div>
        <div className="column">
          <label>Order ID:</label>
          <input value={this.state.orderId} onKeyUp={ this.detectEnter } type="number" onChange={ this.setOrderId } name="order" />
        </div>
        <br />

        <div className={"button"+(this.enableSubmit() ? '' : ' disabled')} onClick={ this.updateUrl }>
          { submit.submitText }
        </div>
      </div>
      <br />
      <br />
      { userTable }
      { runDetails }
      { orderDetails }

      <div className="form">
        <Modal isOpen={ this.state.showVerifyModal } contentLabel="Run Verification">
          <h1>Verify Run</h1>
          <label>Code:</label>
          <input type="text" onChange={evt => this.setState({verifyCode: evt.target.value}) } />
          <br />
          <div className="button button-cancel" onClick={() => this.setState({showVerifyModal: false})}>Cancel</div>
          <div className={'button button-submit' + (this.state.verifyCode ? '' : ' disabled') } onClick={ this.verifyRun }>Verify</div>
        </Modal>
      </div>

      <br />
      <br />
        {
          location.hash.match('/orders/') &&
          <UserOrders 
            userId={ this.props.params.orderUserId }
            displayName={ this.props.params.orderUserDisplay }
            orderLink={ this.renderOrderLink }
            runLink={ this.renderRunLink } />
        }
        {
          location.hash.match('/runs/') &&
          <UserRuns
            userId={ this.props.params.runUserId }
            displayName={ this.props.params.runUserDisplay }
            orderLink={ this.renderOrderLink }
            runLink={ this.renderRunLink } />
        }
    </section>);
  },


  getSubmitFunc: function() {
    var submitFunc, submitText;
    switch (this.state.searchMode) {
      case 'user':
        submitText = "Search Users";
        submitFunc = this.searchUsers;
        break;
      case 'run':
        submitText = "Search Runs";
        submitFunc = this.searchRuns;
        break;
      case 'order':
        submitText = "Search Orders";
        submitFunc = this.searchOrders;
        break;
      default:
        submitFunc = () => App.warn('Invalid URL parameter');
    }

    return {
      submitText,
      submitFunc
    }
  },


  enableSubmit: function() {
    switch (this.state.searchMode) {
      case 'user':
        return this.state.firstName || this.state.lastName || this.state.email || this.state.phone || this.state.userId;
      case 'run':
        return this.state.runId != '';
      case 'order':
        return this.state.orderId != '';
    }
  },


  setFirstName: function(evt) {
    this.setState({
      searchMode: 'user',
      runId: '', orderId: '',
      firstName: evt.target.value
    });
  },


  setLastName: function(evt) {
    this.setState({
      searchMode: 'user',
      runId: '', orderId: '',
      lastName: evt.target.value
    });
  },


  setEmail: function(evt) {
    this.setState({
      searchMode: 'user',
      runId: '', orderId: '',
      email: evt.target.value
    });
  },


  setPhone: function(evt) {
    this.setState({
      searchMode: 'user',
      runId: '', orderId: '',
      phone: evt.target.value
    });
  },


  setUserId: function(evt) {
    this.setState({
      searchMode: 'user',
      runId: '', orderId: '',
      userId: evt.target.value
    });
  },


  setRunId: function(evt, doSearch) {
    this.setState({
      searchMode: 'run',
      firstName: '', lastName: '', email: '', phone: '', userId: '', orderId: '',
      runId: evt.target.value
    }, () => {
      if (doSearch === true) {
        this.updateUrl(); // Triggers search when URL updates
      }
    });
  },


  setOrderId: function(evt, doSearch) {
    this.setState({
      searchMode: 'order',
      firstName: '', lastName: '', email: '', phone: '', userId: '', runId: '',
      orderId: evt.target.value
    }, () => {
      if (doSearch === true) {
        this.updateUrl(); // Triggers search when URL updates
      }
    });
  },


  detectEnter: function(evt) {
    evt = evt || window.event;
    if (evt.keyCode === 13) {
      this.updateUrl();
      return false;
    }
  },


  clearTable: function() {
    this.setState({
      orderUserId: '',
      orderUserDisplay: '',
      runUserId: '',
      runUserDisplay: '',
      runId: '',
      orderId: '',
      users: [],
      run: {},
      order: {}
    });
  },


  updateUrl: function() {
    const state = this.state;
    const params = {};
    if (state.searchMode) params.searchMode = state.searchMode;

    if (state.firstName) params.firstName = state.firstName;
    if (state.lastName) params.lastName = state.lastName;
    if (state.phone) params.phone = state.phone;
    if (state.userId) params.userId = state.userId;
    if (state.email) params.email = state.email;

    if (state.runId) params.runId = state.runId;
    if (state.orderId) params.orderId = state.orderId;

    const hash = encodeURI(JSON.stringify(params));
    window.location.hash = '/users/'+hash; // Triggers component update and fetch
  },


  searchUsers: function() {
    var path = 'dashboard/users?';
    if (this.state.firstName) {
      path += ("fname=" + this.state.firstName)+'&';
    }
    if (this.state.lastName) {
      path += ("lname=" + this.state.lastName)+'&';
    }
    if (this.state.email) {
      path += ("email=" + this.state.email)+'&';
    }
    if (this.state.phone) {
      path += ("phoneNum=" + this.state.phone)+'&';
    }
    if (this.state.userId) {
      path += ("user_id=" + this.state.userId)+'&';
    }
    this.fetch(path);
    
    // Hide the tables when searching a new user
    this.clearTable();
  },


  searchRuns: function() {
    var path = 'dashboard/runs/' + this.state.runId;
    this.fetch(path);
    this.clearTable();
  },


  searchOrders: function() {
    var path = 'dashboard/orders/' + this.state.orderId;
    this.fetch(path);
    this.clearTable();
  },


  verifyRun: function() {
    const run = this.state.run;
    let path = this.clientPath() + 'run/' + run.runId + '/verifyOrderCode';
    path += '?orderCode=' + this.state.verifyCode;

    console.log('Verify: ', path);
    var that = this;
    $.ajax({
      url: path,
      type: 'GET',
      dataType: 'json',
      success: response => {
        console.log(response);
        if (response.result === false) {
          return App.warn('Verification Failed');
        }

        App.notify('Run verified Successfully.');
      },
      error: err => {
        console.error(err);
        App.warn("Order Search failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
      }
    });
  }
});

module.exports = Users;
