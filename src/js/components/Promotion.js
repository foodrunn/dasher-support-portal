var React = require('react');

var Promotion = React.createClass({

  basePath: function() {
    return App.api() + 'v2/';
  },
  

  getInitialState: function() {
    return {
      action: 'givePromo', // Match function name
      promoCode: '',
      userId:  encodeURI(App.user.dasher_user_id),
      beneficiaryUserId: '',
      benefitValue: '',
      incentiveValue: '',
      incentiveReason: ''
    };
  },


  componentWillUpdate: function(nextProps, nextState) {
    // Hardcode the promo code for incentive points
    if (this.state.action !== nextState.action && nextState.action === 'giveIncentivePoints') {
      this.setState({ promoCode: 'CAMPUS_TEAM_INCENTIVE_POINTS' });
    }
    else if (this.state.action !== nextState.action && this.state.action === 'giveIncentivePoints') {
      this.setState({ promoCode: '' });
    }
  },


  //
  // Network fetch functions
  //
  fetch: function(apiPath) {
    console.log('Fetch: ', this.basePath() + apiPath);
    var that = this;
    $.ajax({
      url: this.basePath() + apiPath,
      type: 'POST',
      dataType: 'json',
      success: that.fetchSuccess,
      error: that.fetchError
    });
  },


  fetchSuccess: function(data) {
    if (data.errors) {
      App.warn(data.errors[0]);
    } else {
      App.notify('Promotion succeeded');
      $('section.promotions input').val('');
      this.setState({
        promoCode: '',
        beneficiaryUserId: '',
        benefitValue: '',
        incentiveValue: '',
        incentiveReason: ''
      })
    }
    console.log(data);
  },


  fetchError: function(err) {
    if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors.length) {
      App.warn("Promotion failed:\n" + err.responseJSON.errors[0]);
    }
    if (err.responseJSON && err.responseJSON.error_code) {
      App.warn("Promotion failed:\n" + err.responseJSON.error_code + ': ' + err.responseJSON.message);
    } else {
      App.warn("Promotion request failed.");
      console.log(err);
    }
  },


  //
  // Form update funcitons
  //
  updatePromoCode: function(evt) {
    this.setState({
      promoCode: evt.target.value
    });
  },


  updateUserId: function(evt) {
    this.setState({
      beneficiaryUserId: encodeURI(evt.target.value)
    });
  },


  updateBenefit: function(evt) {
    this.setState({
      benefitValue: evt.target.value
    });
  },


  updateIncentive: function(evt) {
    this.setState({
      incentiveValue: evt.target.value
    });
  },


  updateIncentiveReason: function(evt) {
    this.setState({
      incentiveReason: evt.target.value
    });
  },


  setAction: function(evt) {
    const actionFunciton = evt.target.value
    this.setState({
      action: actionFunciton
    });
  },


  issuePromo: function() {
    this[this.state.action]();
  },


  render: function() {
    const promoCash = this.state.action === 'givePromoCash';
    const giveIncentivePoints = this.state.action === 'giveIncentivePoints';

    return ( <section className = "promotions">
      <h1>Give a Promotion</h1>
      <p>You can give users promotions using the forms below</p>
      <div className="form">
        <div>
          <label>Choose a refund type:</label>
          <div className="select">
            <select onChange={this.setAction}>
              <option value="givePromo">Give user promo code</option>
              <option value="givePromoCash">Give user promo cash</option>
              <option value="giveIncentivePoints">Give user incentive points</option>
            </select>
          </div>
        </div>

        <div className="column">
          <label>Promo Code: <span className="req">*</span></label>
          <input type="text"
            value={ this.state.promoCode }
            disabled={ this.state.action === 'giveIncentivePoints' }
            name = "promo-code"
            onChange={ this.updatePromoCode } />
        </div>

        <div className="column">
          <label>User ID:<span className="req">*</span></label>
          <input type="text"
            name="user-id"
            onChange={ this.updateUserId }/>
        </div>

        <div className={ promoCash ? '' : 'hidden' }>
          <label>Benefit Value: <span className="req" >*</span></label>
          <input type="text"
            name="benefit-value"
            onChange={ this.updateBenefit }/>
        </div>
        <br/>
        <div className={ 'column' + (giveIncentivePoints ? '' : ' hidden') }>
          <label>Incentive Points: <span className="req" >*</span></label>
          <input type="number" min="0"
            name="incentive-points"
            onChange={ this.updateIncentive }/>
        </div>

        <div className={ 'column' + (giveIncentivePoints ? '' : ' hidden') }>
          <label>Incentive Reason: <span className="req" >*</span></label>
          <input type="text"
            name="incentive-reason"
            onChange={ this.updateIncentiveReason }/>
        </div>

        <div className="button" onClick = { this.issuePromo }>Issue Promo</div>
      </div>
    </section>);
  },


  //
  // Available actions
  //
  givePromo: function() {
    if (!this.state.promoCode) {
      return App.warn('You must enter a valid promo code.');
    }
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must enter a User ID.');
    }

    var path =
      'promotion/couponCode/' +
      this.state.promoCode +
      '/apply?dasherUserId=' +
      this.state.userId + 
      '&beneficiaryUserId=' +
      this.state.beneficiaryUserId;
    this.fetch(path);
  },


  givePromoCash: function() {
    if (!this.state.promoCode) {
      return App.warn('You must enter a valid promo code.');
    }
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must enter a user ID.');
    }
    if (!this.state.benefitValue) {
      return App.warn('You must enter a benefit value.');
    }

    var path =
      'promotion/couponCode/' +
      this.state.promoCode +
      '/apply?dasherUserId=' +
      this.state.userId +
      '&benefitValue=' +
      this.state.benefitValue+ 
      '&beneficiaryUserId=' +
      this.state.beneficiaryUserId;
    this.fetch(path);
  },
  

  giveIncentivePoints: function() {
    if (!this.state.promoCode) {
      return App.warn('You must enter a valid promo code.');
    }
    if (!this.state.beneficiaryUserId) {
      return App.warn('You must enter a user ID.');
    }
    if (!this.state.incentiveValue) {
      return App.warn('You must enter an incentive value.');
    }
    if (!this.state.incentiveReason) {
      return App.warn('You must enter a reason for assigning this incentive.');
    }

    var path =
      'promotion/couponCode/' +
      this.state.promoCode +
      '/apply?dasherUserId=' +
      this.state.userId +
      '&benefitValue=' +
      this.state.incentiveValue + 
      '&beneficiaryUserId=' +
      this.state.beneficiaryUserId + 
      '&activityDescription=' +
      this.state.incentiveReason;
    this.fetch(path);
  }
});

module.exports = Promotion;
