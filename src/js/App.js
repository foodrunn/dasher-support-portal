var React = require('react');
var ReactDOM = require('react-dom');
var NavWrapper = require('./components/NavWrapper');
var cookieData = parseCookie('dlc_whoami');
import moment from 'moment-timezone'

sessionStorage.env = process.env.env;
sessionStorage.api = process.env.api;
sessionStorage.menuapi = process.env.menuapi;
sessionStorage.menuviewapi=process.env.menuviewapi;
sessionStorage.user = JSON.stringify({dasher_user_id: cookieData.uid});

var e = sessionStorage.env ? sessionStorage.env : 'prod';
var u = sessionStorage.user ? JSON.parse(sessionStorage.user) : {};
window.App = {
	environment: e,
	user: u,

	api: function() {
		switch (window.App.environment) {
			case 'test':
				return 'testclientapi/';
			case 'stage':
				return 'stageclientapi/';
			case 'local':
				return 'localclientapi/';
			default:
				return 'prodclientapi/';
		}
	},

	supportApi: function() {
		switch (window.App.environment) {
			case 'test':
				return 'testsupportapi/';
			case 'stage':
				return 'stagesupportapi/';
			case 'local':
				return 'localsupportapi/';
			default:
				return 'prodsupportapi/';
		}
	},

	menuapi: function() {
		switch (window.App.environment) {
			case 'test':
				// no menu legacy api in test -- use stage
				return 'menustageapi/';
			case 'stage':
				return 'menustageapi/';
			case 'local':
				return 'menulocalapi/';
			default:
				return 'menuprodapi/';
		}
	},

	usTimeZones: [
		{label:'Pacific', code:'America/Los_Angeles'},
		{label:'Mountain', code:'America/Denver'},
		{label:'Central', code:'America/Chicago'},
		{label:'Eastern', code:'America/New_York'}
	],

	utcToLocal: function(utcTime) {
		var offset = new Date().getTimezoneOffset();
		// var d = new Date(utcTime);
		// d.setMinutes(d.getMinutes() - offset);
		if (!utcTime.match(/Z$/)) {
			utcTime = utcTime + ' Z';
		}
		var d = moment(utcTime).toDate();
		return d.toString().replace(/\sGMT-\d\d\d\d/, '');
	},

	auth: function() {
		return window.App.user.dasher_user_id !== undefined;
	},

	notify: function() {
		// Overload this function
	},

	warn: function() {
		// Overload this function
	},

	closeMessage: function() {
		// Overload this funciton
	}
};


function parseCookie(cName) {
	var cookies = document.cookie.split(';');
	var ret = {};
	for (var c=0; c<cookies.length; c++) {
		var cookie = cookies[c];
		if (cookie.split('=')[0].trim() === cName) {
			var data = cookie.split('=');
			for (var d=0; d<data.length; d+=2) {
				try {
					return JSON.parse(decodeURIComponent(data[d+1]));
				} catch(err) {
					console.error(err);
				}
			}
		}
	}
	return ret;
}


$(document).ajaxError((event, jqxhr, settings, thrownError) => {
	if (thrownError === 'Unauthorized') {
		if (jqxhr.responseJSON && jqxhr.responseJSON.error_code) { // Server auth error
			return App.warn(
				'<h5>'+jqxhr.responseJSON.error_code+'</h5>'+
				'<p>'+jqxhr.responseJSON.message+'</p>'
			);
		}
		App.warn(
			'<span><p>You have been logged out due to inactivity.</p>'+
			'<p>Please refresh the page to log in.</p></span>'
		);
	}
});

ReactDOM.render(<NavWrapper />, document.getElementById('app'));